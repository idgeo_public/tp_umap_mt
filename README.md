# TP_UMAP_MT

Proposition d'un projet Gitlab pour y déposer le résultat de vos travaux. On en profite pour découvrir un outil de collaboration.

-------

## Contexte

Nous profitons de notre découverte de l'outil d'édtion en ligne de carto dynamique [UMAP](https://umap.openstreetmap.fr/fr/) pour réaliser ce petit exercice.

Nous passons par l'outil collaboratif de versionnement Gitlab pour présenter le sujet et également pouvoir y déposer votre restitution sous la forme d'une page wiki.


## Exercice

Sur la *thématique* et l'*étendue géographique* de votre choix nous vous invitons à nous proposer une cartographie dynamique.

> Cette carto doit :

- intégrer des données exogènes de type vecteur (plusieurs couches de différents types, à minima une couche de chaque (point, ligne, polygone)).

- Proposer des analyses thématiques différenciées et/ou personnalisation d'icône.

- Permettre l'édition d'une couche vers des utilisateurs identifés (formateur ou stagaires) et une autre vers un utilisateur public.

> Merci de proposer un page wiki à votre nom décrivant vos choix et le lien vers votre carto.

> Explorer la fonction de données distantes et son usage au niveau d'une couche (nouvelle ou au choix). En faire une explication spécifique dans la note de synthèse (page wiki à votre nom).
